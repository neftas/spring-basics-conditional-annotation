package com.relentlesscoding.spring.conditional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class AppTest {

    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void testConditionalBeanLoading() {
        // change property app.caching.enabled in app.properties and see this method fail
        assertFalse(applicationContext.containsBean("cacheConfig"));
    }
}
