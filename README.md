# Spring Basics: @Conditional

Sample Spring project where the `@Conditional` annotation is demonstrated on
a `@Configuration` class.

There is a single unit test that demonstrates the behavior. Change the
property `app.cache.enabled` in `app.properties` to `true` and see the unit
test fail.

This is part of [this blog post](https://relentlesscoding.com/2018/10/19/spring-basics-conditional-bean-wiring/) published at [relentlesscoding.com](https://relentlesscoding.com).
